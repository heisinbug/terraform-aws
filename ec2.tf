terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
provider "aws" {
  version = "~> 3.0"
  region  = "ap-south-1"
}

resource "aws_instance" "test2" {
    ami = "ami-0b02eacf129bfac4e"
    instance_type = "t2.micro"
    tags = {
      "Name" = "test2"
    }
}
